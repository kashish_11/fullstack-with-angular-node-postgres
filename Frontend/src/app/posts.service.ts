import { Injectable } from "@angular/core";
import { HttpClient} from "@angular/common/http"
import {Get} from './getRequest.model'
@Injectable({providedIn:'root'})
export class PostsService {
    constructor(private http:HttpClient){}

    fetchGetData(){
      return this.http.get<Get[]>
    ('http://localhost:8000/api/todos')
    }

    fetchPostById(id){
      return this.http.get
      ('http://localhost:8000/api/todos/'+id)  
   }


      
}