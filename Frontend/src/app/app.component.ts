import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PostsService } from './posts.service'
import { Get } from './getRequest.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  isFetching = false;
  error = null;
  getRequestData:Get[]=[] ;
  getData = null;
  errorForId;

  constructor(private http: HttpClient,
    private postsService:PostsService
    ) {}

  ngOnInit() {
  }

  getAllPosts(){
    this.isFetching=true;
    this.postsService.fetchGetData().subscribe((response:Get[])=>{
      this.isFetching=false;
      this.getRequestData = response;
      console.log(response);
    })
  }
  getPostById(input:HTMLInputElement){
    console.log()
    this.postsService.fetchPostById(input.value).subscribe((response)=>{
      console.log(response)
      this.getData = response
    },error=>{
      this.errorForId=error.message;
      this.getData = null;
      console.log(error.message)
    })
  }
  onHandleError(){
    this.error=null
  }

}
