

export interface Get{
    id:number;
    title:string;
    todoItems:[{complete:boolean,content:string,createdAt:string,id:number,todoId:number,updatedAt:string}];
    updatedAt: string;
} 